import 'package:flutter/material.dart';
import 'screens/only_page.dart';

void main() => runApp(FirstApp());

class FirstApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: OnlyPage(),
    );
  }
}
