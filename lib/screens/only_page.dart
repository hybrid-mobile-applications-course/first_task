import 'package:flutter/material.dart';
import 'dart:math';
import 'package:toast/toast.dart';

class OnlyPage extends StatefulWidget {
  @override
  _OnlyPageState createState() => _OnlyPageState();
}

Color colorBox1 = Colors.red;
Color colorBox2 = Colors.red;
Color colorBox3 = Colors.red;
Color colorBox4 = Colors.red;

class _OnlyPageState extends State<OnlyPage> {
  @override
  Widget build(BuildContext context) {
    return Material(
        color: Colors.black,
        child: SafeArea(
          child: Container(
            decoration: BoxDecoration(
                border: Border.all(width: 10, color: const Color(0xfff0e3af))),
            child: Row(
              children: <Widget>[
                Expanded(
                  child: Container(
                    color: const Color(0xffC3C3C3),
                    child: Container(
                      margin: const EdgeInsets.only(top: 150.0, bottom: 250),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          OutlineButton(
                            borderSide: BorderSide(
                                color: const Color(0xffB6E61F), width: 3.0),
                            shape: new ContinuousRectangleBorder(),
                            onPressed: () => updateBox(1),
                            child: Text(
                              "Button1",
                              style: TextStyle(
                                  fontSize: 20.0, fontWeight: FontWeight.bold),
                            ),
                          ),
                          OutlineButton(
                            borderSide: BorderSide(
                                color: const Color(0xffB6E61F), width: 3.0),
                            shape: new ContinuousRectangleBorder(),
                            onPressed: () => updateBox(2),
                            child: Text(
                              "Button2",
                              style: TextStyle(
                                  fontSize: 20.0, fontWeight: FontWeight.bold),
                            ),
                          ),
                          OutlineButton(
                            borderSide: BorderSide(
                                color: const Color(0xffB6E61F), width: 3.0),
                            shape: new ContinuousRectangleBorder(),
                            onPressed: () => updateBox(3),
                            child: Text(
                              "Button3",
                              style: TextStyle(
                                  fontSize: 20.0, fontWeight: FontWeight.bold),
                            ),
                          ),
                          OutlineButton(
                            borderSide: BorderSide(
                                color: const Color(0xffB6E61F), width: 3.0),
                            shape: new ContinuousRectangleBorder(),
                            onPressed: () => updateBox(4),
                            child: Text(
                              "Button4",
                              style: TextStyle(
                                  fontSize: 20.0, fontWeight: FontWeight.bold),
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                ),
                Expanded(
                  child: Container(
                    color: const Color(0xffB97A57),
                    child: Container(
                      margin: const EdgeInsets.only(top: 100.0, bottom: 50),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Container(
                            width: 100.0,
                            height: 90.0,
                            decoration: BoxDecoration(
                              color: colorBox1,
                            ),
                            child: Center(
                              child: Text(
                                'BOX1',
                                style: TextStyle(
                                    fontFamily: 'Arial',
                                    fontSize: 22,
                                    fontWeight: FontWeight.bold,
                                    color: Colors.black),
                              ),
                            ),
                          ),
                          Container(
                            width: 100.0,
                            height: 90.0,
                            decoration: BoxDecoration(
                              color: colorBox2,
                            ),
                            child: Center(
                              child: Text(
                                'BOX2',
                                style: TextStyle(
                                    fontFamily: 'Arial',
                                    fontSize: 22,
                                    fontWeight: FontWeight.bold,
                                    color: Colors.black),
                              ),
                            ),
                          ),
                          Container(
                            width: 100.0,
                            height: 90.0,
                            decoration: BoxDecoration(
                              color: colorBox3,
                            ),
                            child: Center(
                              child: Text(
                                'BOX3',
                                style: TextStyle(
                                    fontFamily: 'Arial',
                                    fontSize: 22,
                                    fontWeight: FontWeight.bold,
                                    color: Colors.black),
                              ),
                            ),
                          ),
                          Container(
                            width: 100.0,
                            height: 90.0,
                            decoration: BoxDecoration(
                              color: colorBox4,
                            ),
                            child: Center(
                              child: Text(
                                'BOX4',
                                style: TextStyle(
                                    fontFamily: 'Arial',
                                    fontSize: 22,
                                    fontWeight: FontWeight.bold,
                                    color: Colors.black),
                              ),
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                )
              ],
            ),
          ),
        ));
  }

  void showLastColor(int boxNumber, Color lastColor, Color newColorName) {
    String colorName = lastColor
        .toString()
        .substring(39, lastColor.toString().length - 2)
        .toUpperCase();
    String changedColorName = newColorName
        .toString()
        .substring(39, newColorName.toString().length - 2)
        .toUpperCase();

    Toast.show(
        "Box $boxNumber color changed \n"
        "$colorName - Hex code for previous color\n"
        "$changedColorName - Hex code for new color",
        context,
        duration: Toast.LENGTH_LONG,
        gravity: Toast.CENTER,
        backgroundColor: Colors.tealAccent[100],
        textColor: Colors.black);
  }

  void updateBox(int boxNumber) {
    Color prevColor;
    Color newColor =
        Colors.primaries[Random().nextInt(Colors.primaries.length)];
    setState(() {
      switch (boxNumber) {
        case 1:
          {
            prevColor = colorBox1;
            colorBox1 = newColor;
            showLastColor(1, prevColor, colorBox1);
          }
          break;
        case 2:
          {
            prevColor = colorBox2;
            colorBox2 = newColor;
            showLastColor(2, prevColor, colorBox2);
          }
          break;
        case 3:
          {
            prevColor = colorBox3;
            colorBox3 = newColor;
            showLastColor(3, prevColor, colorBox3);
          }
          break;
        case 4:
          {
            prevColor = colorBox4;
            colorBox4 = newColor;
            showLastColor(4, prevColor, colorBox4);
          }
          break;
      }
    });
  }
}
